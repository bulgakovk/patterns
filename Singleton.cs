﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class City
    {
        private static City instance;
        private string name;
        private static List<String> citizenList;

        public static List<String> CitizenList
        {
            get { return citizenList; }
        }

        private City (string cityName)
        {
            this.name = cityName;
            citizenList = new List<String>();
        }

        public static City getInstance (string cityName)
        {
            if (instance == null)
            {
                instance = new City(cityName);
            }

            return instance;
        }

        public static void addPerson (string personName)
        {
            citizenList.Add(personName);
        }

    }
}
